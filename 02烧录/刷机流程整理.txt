1.RK3586烧录流程
	1.1 安装驱动
	1.2 打开 RKDevTool.exe 程序，按住reset键和recover键，然后松开reset，直到显示
	"发现一个 LOADER 设备", 说明开发板进入 Loader 模式等待烧写固件
	（可能失败原因是虚拟机USB提示连接到主机，设置去掉提示即可）
	1.3 interactiveScript_rk3568.bat脚本用于推hap包(App)和so文件
	1.4 安装SecureCRT 9.1，需先破解，该软件供调试使用  生成日志 hilog -w start
	日志输出路径/data/log/hilog 
	1.5 secureCRT 波特率设置1500000
	
2.Hi3516烧录流程
	2.1安装usb驱动，安装补丁，安装usb转串口驱动
	2.2Hitool烧录程序 选择 emmc烧录和usb烧录 
	2.3 按住主板上面的update按键（串口旁边），然后插拔USB线，待出现绿色的进度条即可
	2.4 secureCRT 波特率设置115200