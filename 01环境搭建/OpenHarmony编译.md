## 一、环境安装
1. https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-package-environment.md
2. https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3516-setting.md


## 二、源代码获取
https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-sourcecode-acquire.md

## 三、需要额外安装的软件：

- flex
- bison


## 四、编译

1. 编译 L2 获取 prebuilts：
```shell
bash build/prebuilts_download.sh # 下载并解压 prebuilts 压缩包到指定位置
```

2. L2 编译命令：
```shell
./build.sh --product-name Hi3516DV300  
./build.sh --product-name Hi3516DV300 --ccache
./build.sh --product-name Hi3516DV300 --export-para BUILD_AOSP:false --build-target dsoftbus_standard
```

## F&Q（持续更新

### 1. 异常情况不知道怎么具体怎么解决，编译问题三板斧用起来！
- 删除 out

```shell
rm -rf out
```
- 更新工具链：
```shell
./build/prebuilts_download.sh
```
- 更新二进制和代码：
```shell
# 二进制更新
repo forall -c "git lfs pull"
# 代码更新
repo sync -c --no-tags
```
> `只要门禁没问题，有问题的就是你`，以上能解决 90%的门禁问题 —— OH知名老专家 /手动滑稽

